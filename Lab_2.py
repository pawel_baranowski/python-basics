# Laboratorium nr 2

# III. Złożone typy danych
# 1. krotka, lista, słownik
# 2. podstawowe operacja na listach
# 3. rozpakowywanie krotki/listy

# listy

lista = [1, 4, 15, 3]
lista = [1, 4, 15, 3,]    # to samo, ale nie powoduje bledu!

# odwolywanie sie do elementow listy
print(lista[0] + lista[1])


# UWAGA - lista moze zawierac rozne typy!
# oraz wyrazenia np.

a = 10
lista_r = [1, "napis", a, a*9]

print(type(lista_r[0]))
print(type(lista_r[1]))

listalist = [[1, 2, 5], [0, 1], [True, False]]

lista_5 = [1, 2, 4]
lista_5.append(9)

len(lista_5)

lista_5.append([1, 0])

len(lista_5)
lista_5[4]

# slice z listy (krótko)
lista_5[2:4]
lista_5[2:]

# listy można "dodawać" (złączać) - NIE JAK WEKTORY

[1, 2, 3] + [10, 15, 20]

# listy można "mnożyć" (zwielokrotniać) - NIE JAK WEKTORY

[1, 2, 3] * 4

# krotka
krotka = (1, 5)
krotka[0]
krotka[1]

a, b = krotka

listanowa = list(krotka)
c, d = krotka

# slownik

dictionary = {'PESEL': 95020700678, 'ocena': 4.5, 'komentarz': '')





